import './App.css'
import { useState } from 'react'
import { ethers } from 'ethers'
import Greeter from './artifacts/contracts/Greeter.sol/Greeter.json'
import VNToken from './artifacts/contracts/VNToken.sol/VNToken.json'

const greeterAddress = '0xBd71607Abbc04cB81ab6DB9c6D2dd166853f9B86'
const vntokenAddress = '0x21610F96b401a0FC91a7Eb90794ca90dbc3810a4'

function App() {
  const [greeting, setGreetingValue] = useState('')
  const [receivingAddress, setReceivingAddress] = useState('')
  const [amount, setAmount] = useState(0)

  async function requestAccount() {
    await window.ethereum.request({ method: 'eth_requestAccounts' })
  }

  async function fetchGreeting() {
    if (typeof window.ethereum !== 'undefined') {
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      console.log({ provider })
      const contract = new ethers.Contract(
        greeterAddress,
        Greeter.abi,
        provider
      )
      try {
        const data = await contract.greet()
        console.log('data: ', data)
      } catch (err) {
        console.log('Error: ', err)
      }
    }
  }

  async function setGreeting() {
    if (!greeting) return
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      console.log({ provider })
      const signer = provider.getSigner()
      const contract = new ethers.Contract(greeterAddress, Greeter.abi, signer)
      const transaction = await contract.setGreeting(greeting)
      setGreetingValue('')
      await transaction.wait()
      fetchGreeting()
    }
  }

  async function getBalance() {
    if (typeof window.ethereum !== 'undefined') {
      const [account] = await window.ethereum.request({
        method: 'eth_requestAccounts',
      })
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(
        vntokenAddress,
        VNToken.abi,
        provider
      )
      const balance = await contract.balanceOf(account)
      console.log('Balance: ', balance.toString())
    }
  }

  async function sendCoins() {
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const signer = provider.getSigner()
      const contract = new ethers.Contract(vntokenAddress, VNToken.abi, signer)
      const convertedAmount = `${parseFloat(amount) * 10 ** 18}`
      const transation = await contract.transfer(
        receivingAddress,
        convertedAmount
      )
      setReceivingAddress('')
      setAmount(0)
      await transation.wait()
      console.log(`${amount} Coins successfully sent to ${receivingAddress}`)
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <input
          onChange={(e) => setGreetingValue(e.target.value)}
          placeholder="Set greeting"
          value={greeting}
        />
        <div style={{ display: 'flex' }}>
          <button onClick={fetchGreeting}>Fetch Greeting</button>
          <button onClick={setGreeting}>Set Greeting</button>
        </div>

        <br />
        <input
          onChange={(e) => setReceivingAddress(e.target.value)}
          placeholder="Account ID"
          value={receivingAddress}
        />
        <input
          onChange={(e) => setAmount(e.target.value)}
          placeholder="Amount"
          value={amount}
        />
        <div style={{ display: 'flex' }}>
          <button onClick={getBalance}>Get Balance</button>
          <button onClick={sendCoins}>Send Coins</button>
        </div>
      </header>
    </div>
  )
}

export default App
